import React from 'react';
import { Product } from '../../../model/Product';

interface ProductsFormProps {
  onSubmit: (product: Partial<Product>) => void;
}

export const ProductsForm: React.FC<ProductsFormProps> = props => {

  function addTodoHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      props.onSubmit({
        title: e.currentTarget.value,
        price: 10,
      })
      e.currentTarget.value = '';
    }
  }

  return (
    <input
      type="text"
      onKeyPress={addTodoHandler}
      className="form-control"
    />
  );
};
