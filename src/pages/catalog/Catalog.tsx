import React, { useEffect, useLayoutEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/products/products.actions';
import { selectProducts, selectTotal } from './store/products/product.selectors';
import { Product } from '../../model/Product';
import { ProductsList } from './components/ProductList';
import { ProductsForm } from './components/ProductForm';

export const Catalog: React.FC = () => {
  const dispatch = useDispatch();
  const products = useSelector(selectProducts)
  const total = useSelector(selectTotal)

  useEffect(() => {
    dispatch(getProducts())
  }, [dispatch])

  function addTodoHandler(p: Partial<Product>) {
    dispatch(addProduct(p))
      // .then()
  }

  return (
    <div>
      <ProductsForm
        onSubmit={addTodoHandler} />
      <ProductsList
        total={total}
        products={products}
        onToggle={(p) =>  dispatch(toggleProduct(p))}
        onDelete={(id) =>  dispatch(deleteProduct(id))}
      />
    </div>
  )
}

