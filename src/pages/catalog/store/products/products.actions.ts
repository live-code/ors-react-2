import Axios from 'axios';

import { AppThunk } from '../../../../App';
import { addProductSuccess, getProductsSuccess, toggleProductVisibility, deleteProductSuccess } from './products.store';
import { Product } from '../../../../model/Product';

// no arrow syntax
export const getProducts = function(): AppThunk {
  return async function (dispatch) {
    try {
      const response = await Axios.get<Product[]>('http://localhost:3001/products')
      dispatch(getProductsSuccess(response.data))
    } catch (err) {
      // dispatch error
    }
  }
}

// with arrow syntax
export const getProducts2 = (): AppThunk => async dispatch => {

  try {
    const response = await Axios.get<Product[]>('http://localhost:3001/products')
    dispatch(getProductsSuccess(response.data))
  } catch (err) {
    // dispatch error
  }
};


export const deleteProduct = (id: number | undefined): AppThunk => async dispatch => {
  try {
    await Axios.delete(`http://localhost:3001/products/${id}`);
    if (id) {
      dispatch(deleteProductSuccess(id))
    }
  } catch (err) {
    // dispatch error
  }
};

export const addProduct = (product: Partial<Product>): AppThunk => async dispatch => {

  try {
    const newProduct = await Axios.post<Product>('http://localhost:3001/products', {
        ...product,
        visibility: false
      }
    );
    dispatch(addProductSuccess(newProduct.data))
  } catch (err) {
    // dispatch error
  }
};

export const toggleProduct = (product: Partial<Product>): AppThunk => async dispatch => {
  try {
    const updatedProduct: Partial<Product> = {
      ...product,
      visibility: !product.visibility
    };
    const response = await Axios.patch<Product>(`http://localhost:3001/products/${product.id}`, updatedProduct);
    dispatch(toggleProductVisibility(response.data))
  } catch (err) {
    // dispatch error
  }
};
