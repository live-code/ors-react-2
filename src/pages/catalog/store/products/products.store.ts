import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../../../../model/Product';

export const productsStore = createSlice({
  name: 'products',
  initialState: [] as Partial<Product>[],
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      return action.payload;
    },
    addProductSuccess(state, action: PayloadAction<Partial<Product>>) {
      state.push(action.payload);
      // return [...state, action.payload];
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      const index = state.findIndex(p => p.id === action.payload)
      state.splice(index, 1);
      // return state.filter(p => p.id !== action.payload)
    },
    toggleProductVisibility(state, action: PayloadAction<Product>) {
      const product = state.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
    },
  },
  extraReducers: {
  }
})

export const {
  addProductSuccess, deleteProductSuccess, getProductsSuccess, toggleProductVisibility
} = productsStore.actions
