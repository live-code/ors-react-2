import { RootState } from '../../../../App';

/*
// filtered
export const selectProducts = (state: RootState) =>
  state.catalog.list.filter(p => {
    if (p.price) {
      return p.price > state.catalog.filters.price
    }
  });*/

export const selectProducts = (state: RootState) => state.catalog.list;

export const selectTotal = (state: RootState) =>
  state.catalog.list.reduce((acc, curr) => acc + (curr.price || 0), 0);
