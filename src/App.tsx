import { Action, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import React from 'react';
import { Provider } from 'react-redux';
import { Catalog } from './pages/catalog/Catalog';
import { productsStore } from './pages/catalog/store/products/products.store';
import { catalogReducers } from './pages/catalog/store';

const rootReducer = combineReducers({
  catalog: catalogReducers
})

const store = configureStore({
  reducer: rootReducer
})

export type RootState = ReturnType<typeof rootReducer>

export type AppThunk = ThunkAction<void, RootState, null, Action<string> >
export default function App() {
  console.log('------\nApp: render')

  return (
    <Provider store={store}>
      <Catalog />
    </Provider>
  );
}
